#!/bin/bash

####################################
# @description 离线自动安装mysql5.7
#                 1.建立临时文件夹
#                 2.解压zip文件 -- 下载地址 https://www.aliyundrive.com/s/JZAFEwfUUC1  下载后，与此脚本放同一目录
#                 3.删除centos7自带的MySQL
#                 4.按顺序安装MySQL相关的依赖包
#                 5.修改MySQL的密码 -- 默认使用密码为root
#                 6.配置MySQL可以被远程访问（实际开发中也是，有些机器需要有访问这个机器MySQL的权限）
#                 7.开放3306端口，提供远程访问
# @params $? => 代表上一个命令执行后的退出状态: 0->成功,1->失败
# @example => sh mysql5.7.sh
# @author zhengqingya
# @date 2022/2/11 9:47
####################################


#第一步、创建一个临时的文件夹
mkdir mysql_soft


#第二步、把新上传的MySQL的安装包移动到mysql_soft下
mv mysql57.zip mysql_soft


#进入到mysql_soft目录下，开始执行下面的操作
cd mysql_soft


#解压MySQL的安装包，在当前目录下
unzip mysql57.zip


#查看集群上现有的MySQL，需要删除掉
mysql_old=`rpm -qa | grep mariadb`
echo -e "当前centos中MySQL版本是：   ${mysql_old}"

#删除自带的MySQL
rpm -e --nodeps ${mysql_old}


#验证旧版本的MySQL是否被删除掉
mysql_validate=`rpm -qa | grep mariadb`


echo -e "验证旧版的MySQL是否被删除干净：   ${mysql_validate}"


#开始安装MySQL，注意：这些包安装是有顺序的
rpm -ivh libaio-0.3.109-13.el7.x86_64.rpm

#判断安装的过程是否成功
if [ $? == 0 ];then
	echo -e "安装------libaio-0.3.109-13.el7.x86_64.rpm  --  第1个包------- 成功"
else
	echo -e "安装------libaio-0.3.109-13.el7.x86_64.rpm  --  第1个包------- 失败"
fi

#第二个包
rpm -ivh perl-Data-Dumper-2.145-3.el7.x86_64.rpm


if [ $? == 0 ];then
        echo -e "安装------perl-Data-Dumper-2.145-3.el7.x86_64.rpm  --  第2个包------- 成功"
else
        echo -e "安装------perl-Data-Dumper-2.145-3.el7.x86_64.rpm  --  第2个包------- 失败"
fi


#第三个包：安装rpm -ivh numactl* 这是安装以前缀numactl开始的所有安装包
rpm -ivh numactl*


if [ $? == 0 ];then
        echo -e "安装------numactl*  --  第3个包------- 成功"
else
        echo -e "安装------numactl*  --  第3个包------- 失败"
fi


#第四个包：解压mysql-5.7.26-1.el7.x86_64.rpm-bundle.tar
tar -xf mysql-5.7.26-1.el7.x86_64.rpm-bundle.tar

if [ $? == 0 ];then
        echo -e "解压------mysql-5.7.26-1.el7.x86_64.rpm-bundle.tar  ------- 成功"
else
        echo -e "解压------mysql-5.7.26-1.el7.x86_64.rpm-bundle.tar  ------- 失败"
fi


#第四个包：安装 mysql-community-common-5.7.26-1.el7.x86_64.rpm
rpm -ivh mysql-community-common-5.7.26-1.el7.x86_64.rpm

if [ $? == 0 ];then
        echo -e "安装------mysql-community-common-5.7.26-1.el7.x86_64.rpm  --  第4个包------- 成功"
else
        echo -e "安装------mysql-community-common-5.7.26-1.el7.x86_64.rpm  --  第4个包------- 失败"
fi

#第五个包：安装mysql-community-libs-*
rpm -ivh mysql-community-libs-*

if [ $? == 0 ];then
        echo -e "安装------mysql-community-libs-*  --  第5个包------- 成功"
else
        echo -e "安装------mysql-community-libs-*  --  第5个包------- 失败"
fi

#第六个包：安装mysql-community-devel-5.7.26-1.el7.x86_64.rpm
rpm -ivh mysql-community-devel-5.7.26-1.el7.x86_64.rpm
if [ $? == 0 ];then
        echo -e "安装------mysql-community-devel-5.7.26-1.el7.x86_64.rpm  --  第6个包------- 成功"
else
        echo -e "安装------mysql-community-devel-5.7.26-1.el7.x86_64.rpm  --  第6个包------- 失败"
fi

#第七个包：安装net-tools-2.0-0.25.20131004git.el7.x86_64.rpm
rpm -ivh net-tools-2.0-0.25.20131004git.el7.x86_64.rpm
if [ $? == 0 ];then
        echo -e "安装------net-tools-2.0-0.25.20131004git.el7.x86_64.rpm  --  第7个包------- 成功"
else
        echo -e "安装------net-tools-2.0-0.25.20131004git.el7.x86_64.rpm  --  第7个包------- 失败"
fi

#第八个包：安装mysql-community-client-5.7.26-1.el7.x86_64.rpm
rpm -ivh mysql-community-client-5.7.26-1.el7.x86_64.rpm
if [ $? == 0 ];then
        echo -e "安装------mysql-community-client-5.7.26-1.el7.x86_64.rpm  --  第8个包------- 成功"
else
        echo -e "安装------mysql-community-client-5.7.26-1.el7.x86_64.rpm  --  第8个包------- 失败"
fi

#第九个包：安装mysql-community-server-5.7.26-1.el7.x86_64.rpm
rpm -ivh mysql-community-server-5.7.26-1.el7.x86_64.rpm
if [ $? == 0 ];then
        echo -e "安装------mysql-community-server-5.7.26-1.el7.x86_64.rpm  --  第9个包------- 成功"
else
        echo -e "安装------mysql-community-server-5.7.26-1.el7.x86_64.rpm  --  第9个包------- 失败"
fi

#启动MySQL
service mysqld start

#验证是否启动成功可以查看MySQL的启动状态
service mysqld status >start.log

#查看MySQL的初始密码,把携带密码信息加载到下面这个文件中
grep 'temporary password' /var/log/mysqld.log >result.txt


#读取文件中的密码，用于登录MySQL
password_mysql=`cat result.txt | grep 'localhost' | awk -F ': ' '{print $2}'`
echo -e "查看MySQL的原始密码是啥？    ${password_mysql}"

#连接MySQL的几个参数
hostname="localhost"
#hostname="www.zhengqingya.com"
username="root"
port="3306"

#进入MySQL的交互模式，修改相应的参数，设置简单的密码为root，为了以后自己用方便，实际上生产的密码都比较复杂一些
#mysql -h${hostname} -u${username} -P${port} -p${password_mysql} <<EOF
mysql  -u${username} -p${password_mysql} --connect-expired-password <<EOF
	set global validate_password_policy=0;
	set global validate_password_length=1;
	alter user 'root'@'localhost' identified by 'root';


	quit

EOF


new_password="root" #这里是配置远程访问策略
mysql -h${hostname} -u${username} -P${port} -p${new_password} <<EOF
        GRANT ALL PRIVILEGES ON *.*TO 'root'@'%' IDENTIFIED BY 'root';
	flush privileges;

        quit

EOF


openPort(){
  echo -e "开放3306端口"
  firewall-cmd --zone=public --add-port=3306/tcp --permanent
  echo -e "重启firewall..."
  firewall-cmd --reload
}
openPort

echo "|****************************************************************************************************************|"
echo "|            WW             WW EEEEEEE LL     CCCCC   OOOOOO      MM      MM     EEEEEEE                         |"
echo "|             WW    WWWW   WW  EE      LL    CC      OO    OO    MMMM    MMMM    EE                              |"
echo "|              WW  WW WW  WW   EEEEE   LL   CC      OO      OO  MM  MM  MM  MM   EEEEE                           |"
echo "|               WW W   W WW    EE      LL    CC      OO    OO  MM    M M     MM  EE                              |"
echo "|                WW     WW     EEEEEEE LLLLLL CCCCC   OOOOOO  MM     MMM      MM EEEEEEE                         |"
echo "|****************************************************************************************************************|"
