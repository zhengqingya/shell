# 存放各类shell脚本

## Git

| 脚本名称                                       | 脚本简介                   |
| ---------------------------------------------- | -------------------------- |
| [git-move.sh](git/git-move.sh) | git仓库迁移(所有分支) |
| [master-merge-dev.sh](git/master-merge-dev.sh) | git合并dev分支到master分支 |

## Jenkins

| 脚本名称                         | 脚本简介        |
| -------------------------------- | --------------- |
| [jenkins.sh](jenkins/jenkins.sh) | jenkins运行脚本 |

## Liunx

| 脚本名称                         | 脚本简介        |
| -------------------------------- | --------------- |
| [get-random-port.sh](liunx/get-random-port.sh) | 获取一个指定区间内未被占用的随机端口号 |
| [log_size_show.sh](liunx/log/log_size_show.sh) | 查看日志大小 |
| [log_clean.sh](liunx/log/log_clean.sh) | 清理日志 |
| [mysql5.7.sh](liunx/mysql/mysql5.7.sh) | 离线自动安装mysql5.7 |

## Docker

| 脚本名称                         | 脚本简介        |
| -------------------------------- | --------------- |
| [docker_log_size_show.sh](docker/docker_log_size_show.sh) | 查看docker容器日志大小 |
| [docker_log_clean.sh](docker/docker_log_clean.sh) | 清理docker容器日志 |
| [docker-rename-push-images.sh](docker/docker-rename-push-images.sh) | 一键重命名镜像并推送 |

---

> If you don't want to be abandoned by life, then love life, despite the bumpy road ahead.
