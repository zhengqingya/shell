# shell运算符

> [test.sh](09-test.sh)

```shell
sum=`expr 2 + 3`
echo "和: ${sum}" # 5
```

1. 表达式需用``包裹
2. 表达式和运算符两边需有空格

### 算术运算符

| 运算符 | 说明                                          |
| :----- | :-------------------------------------------- |
| +      | 加法                                          |
| -      | 减法                                          |
| *      | 乘法                                          |
| /      | 除法                                          |
| %      | 取余                                          |
| =      | 赋值                                          |
| ==     | 相等。用于比较两个数字，相同则返回 true。     |
| !=     | 不相等。用于比较两个数字，不相同则返回 true。 |

### 关系运算符

| 运算符 | 说明     |
| :----- | :------- |
| -eq    | 等于     |
| -ne    | 不等于   |
| -gt    | 大于     |
| -ge    | 大于等于 |
| -lt    | 小于     |
| -le    | 小于等于 |

### 布尔运算符

| 运算符 | 说明                                                |
| :----- | :-------------------------------------------------- |
| !      | 非运算，表达式为 true 则返回 false，否则返回 true。 |
| -o     | 或运算，有一个表达式为 true 则返回 true。           |
| -a     | 与运算，两个表达式都为 true 才返回 true。           |

### 逻辑运算符

| 运算符 | 说明       |
| :----- | :--------- |
| &&     | 逻辑的 AND |
| \|\|   | 逻辑的 OR  |

### 字符串运算符

| 运算符 | 说明                |
| :----- | :------------------ |
| =      | 等于                |
| !=     | 不等于              |
| -z     | 字符串长度是否为0   |
| -n     | 字符串长度是否不为0 |
| $      | 字符串是否为空      |

### 文件测试运算符

| 操作符  | 说明                                         |
| :------ | :------------------------------------------- |
| -b file | 是否是块设备文件                             |
| -c file | 是否是字符设备文件                           |
| -d file | 是否是目录                                   |
| -f file | 是否是普通文件（既不是目录，也不是设备文件） |
| -g file | 是否设置了 SGID 位                           |
| -k file | 是否设置了粘着位(Sticky Bit)                 |
| -p file | 是否是有名管道                               |
| -u file | 是否设置了 SUID 位                           |
| -r file | 是否可读                                     |
| -w file | 是否可写                                     |
| -x file | 是否可执行                                   |
| -s file | 是否不为空（文件大小是否大于0）              |
| -e file | （包括目录）是否存在                         |
