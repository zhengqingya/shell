#!/bin/bash

echo "================= ↓↓↓↓↓↓ echo输出文本 ↓↓↓↓↓↓ ================="

# echo会自动在末尾添加换行符
# 输出普通字符串
echo "Hello World !"
echo Hello World !

# 输出转义字符
echo "\"Hello World !\""

# 显示变量
name=zhengqingya
echo ${name}

# 显示换行
echo -e "Hi ! \n" # -e 开启转义
echo "嗨!"

# 显示不换行
echo -e "Hello ! \c" # -e 开启转义 \c 不换行
echo "你好"

# 输出至文件
echo "hi" > hi.txt
echo ">:重定向文件内容   >>:追加内容" >> hi.txt

# 原样输出字符串，不进行转义或取变量(用单引号)
echo '$name\"'

# 显示命令执行结果
echo `date +"%Y-%m-%d %H:%M:%S"`



printf "================= ↓↓↓↓↓↓ printf输出文本 ↓↓↓↓↓↓ =================\n"

# printf需手动在末尾添加\n实现像echo一样的换行
printf "Hello, Shell\n"

# %s %c %d %f 都是格式替代符，％s 输出一个字符串，％d 整型输出，％c 输出一个字符，％f 输出实数，以小数形式输出。
# %-10s 指一个宽度为 10 个字符（- 表示左对齐，没有则表示右对齐），任何字符都会被显示在 10 个字符宽的字符内，如果不足则自动以空格填充，超过也会将内容全部显示出来。
# %-4.2f 指格式化为小数，其中 .2 指保留2位小数。
printf "%-10s %-9s %-4s\n" 姓名 性别 体重kg
printf "%-10s %-8s %-4.2f\n" 张三 男 66.1234
printf "%-10s %-8s %-4.3f\n" 李四 男 88.8888
printf "%-10s %-8s %d\n" 王二 男 55
# 如果格式只指定了一个参数，但多出的参数仍然会按照该格式输出，format-string 被重用
printf "%s %s %s\n" a b c d e f g hi j
# 如果没有arguments，那么 %s 用NULL代替，%d 用 0 代替
printf "%s and %d \n"
