# shell函数

# 定义函数
function hi() {
    echo "hello world!"
}

# 执行函数
hi

getNum(){
    echo "这是一个有返回值的函数"
    return 6
}
getNum
# $?获取函数返回值
echo "result: $?"


# 传参给函数
printNum(){
  echo "第1个参数： ${1}"
  echo "第2个参数： ${2}"
}
printNum 66 888
