#!/bin/bash

# 声明数组
array_name=(q1 q2 q3)
# 单个定义
array_name[10]=q10

# 取值-根据下标取单个
echo ${array_name[0]} # q1
# 取值-全部
echo ${array_name[@]} # q1 q2 q3 q10

# 获取数组元素个数
echo ${#array_name[@]} # 4
# 或
echo ${#array_name[*]} # 4
# 获取数组单个元素的长度
echo ${#array_name[10]} # 3
