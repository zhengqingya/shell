# 定时任务

```shell
# 查看crontab服务状态
service crond status
# 启动服务
service crond start
# 关闭服务 
service crond stop
# 重启服务
service crond restart
# 重新载入配置
service crond reload
```

---


vim /etc/crontab

```shell
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root

# For details see man 4 crontabs

# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name  command to be executed


# 定时提交到gitee -> 测试 (每天0点和9点的01分执行1次)
1 0,9 * * * root sh /zhengqingya/soft/timer-task/git-timer-push-gitee/git-timer-push-gitee.sh >> /zhengqingya/soft/timer-task/git-timer-push-gitee/log.txt
```
